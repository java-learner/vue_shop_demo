var express = require('express')
var compression = require('compression')
var app = express()

app.use(compression())
app.use(express.static('./dist'))


app.listen(80,()=>{
	console.log('服务已经启动 http://127.0.0.1')
})

